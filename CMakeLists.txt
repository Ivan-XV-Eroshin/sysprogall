cmake_minimum_required(VERSION 3.15)
project(WinApp)

set(CMAKE_CXX_STANDARD 14)

add_executable(WinApp src/main.cpp
        src/menu/menu.cpp
        src/ApplicationStruct.h)