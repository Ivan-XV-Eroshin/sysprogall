#ifndef WINAPP_APPLICATIONSTRUCT_H
#define WINAPP_APPLICATIONSTRUCT_H


#include <cstdint>

struct application_struct{
    uint8_t brush;
    uint8_t color;
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    HINSTANCE hInstance;
}applicationStruct;

#define RED 1
#define GREEN 2
#define BLUE 3

#define BRUSH_CALLIGRAPHY_PEN 0x01
#define BRUSH_SPRAY_CAN 0x02

#endif //WINAPP_APPLICATIONSTRUCT_H
