#include "menu.h"

void createMenu(HWND hWnd) {
    HMENU hMenu = CreateMenu();
    HMENU hColorMenu = CreatePopupMenu();
    HMENU hBrushMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hBrushMenu, "Brush");
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hColorMenu, "Color");
    AppendMenu(hColorMenu, MF_STRING, 1, "Red");
    AppendMenu(hColorMenu, MF_STRING, 2, "Green");
    AppendMenu(hColorMenu, MF_STRING, 3, "Blue");
    AppendMenu(hBrushMenu, MF_STRING, 4, "Calligraphy Pen");
    AppendMenu(hBrushMenu, MF_STRING, 5, "Spray Can");
    AppendMenu(hBrushMenu, MF_STRING, 6, "RoundPen");
    AppendMenu(hMenu, MF_STRING, 7, "About");
    SetMenu(hWnd, hMenu);
    SetMenu(hWnd, hMenu);
}

