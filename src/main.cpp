#include "main.h"

ATOM RegMyWindowClass(HINSTANCE, LPCTSTR);

LRESULT CALLBACK WndProc(HWND hWnd,
                         UINT message,
                         WPARAM wParam,
                         LPARAM lParam);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow) {
    applicationStruct.color = RED;
    applicationStruct.hInstance = hInstance;
    applicationStruct.brush = BRUSH_CALLIGRAPHY_PEN;
    LPCTSTR lpzClass = TEXT("Simple Window Application");
    if (!RegMyWindowClass(hInstance, lpzClass))
        return 1;
    RECT screen_rect;
    GetWindowRect(GetDesktopWindow(), &screen_rect);
    int x = (screen_rect.right - WINDOW_WIDTH) >> 1;
    int y = (screen_rect.bottom - WINDOW_HEIGHT) >> 1;
    HWND hWnd = CreateWindow(lpzClass,
                             "Simple Application",
                             WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                             x, y, WINDOW_WIDTH, WINDOW_HEIGHT,
                             NULL, NULL,
                             hInstance, NULL);
    createMenu(hWnd);
    MSG msg = {0};    // структура сообщения

    int applicationRunningState = 0;
    do {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        applicationRunningState = GetMessage(&msg, nullptr, NULL, NULL);
    } while (applicationRunningState > 0);
    if (applicationRunningState == -1)
        return 3;

    return msg.wParam;
}

// функция регистрации класса окон
ATOM RegMyWindowClass(HINSTANCE hInst, LPCTSTR lpzClassName) {
    WNDCLASS wcWindowClass = {0};
    wcWindowClass.lpfnWndProc = (WNDPROC) WndProc;// адрес ф-ции обработки сообщений
    wcWindowClass.style = CS_HREDRAW | CS_VREDRAW;// стиль окна
    wcWindowClass.hInstance = hInst;  // дискриптор экземпляра приложения
    wcWindowClass.lpszClassName = lpzClassName; // название класса
    wcWindowClass.hCursor = LoadCursor(NULL, IDC_CROSS);   // загрузка курсора
    wcWindowClass.hbrBackground = (HBRUSH) COLOR_APPWORKSPACE;   // загрузка цвета окон
    return RegisterClass(&wcWindowClass); // регистрация класса
}

void Draw(LPPOINT lpPoint, HDC *hdc);

void UpdateWindow(HWND hWnd, LPRECT lpRect) {
    GetWindowRect(hWnd, lpRect);
    InvalidateRect(hWnd, lpRect, false);
}

LRESULT CALLBACK WndProc(HWND hWnd,
                         UINT message,
                         WPARAM wParam,
                         LPARAM lParam) {
    POINT point;
    HDC hdc;
    PAINTSTRUCT paintStruct;
    RECT rect;
    switch (message) {
        case WM_PAINT: {
            if (wParam == PUSHED) {
                GetCursorPos(&point);
                MapWindowPoints(HWND_DESKTOP, hWnd, &point, 1);
                hdc = BeginPaint(hWnd, &paintStruct);
                Draw(&point, &hdc);
                GetWindowRect(hWnd, &rect);
                ValidateRgn(hWnd, NULL);
                EndPaint(hWnd, &paintStruct);
                UpdateWindow(hWnd, &rect);
            }
            break;
        }
        case WM_COMMAND:
            switch (wParam) {
                case RED:
                    applicationStruct.red = 255;
                    applicationStruct.green = 0;
                    applicationStruct.blue = 0;
                    break;
                case BLUE:
                    applicationStruct.red = 0;
                    applicationStruct.green = 0;
                    applicationStruct.blue = 255;
                    break;
                case GREEN:
                    applicationStruct.red = 0;
                    applicationStruct.green = -1;
                    applicationStruct.blue = 0;
                    break;
                case 4:
                    applicationStruct.brush = BRUSH_CALLIGRAPHY_PEN;
                    break;
                case 5:
                    applicationStruct.brush = BRUSH_SPRAY_CAN;
                    break;
                case 7:

                    HWND hInnerWindow = CreateWindowEx(WS_EX_WINDOWEDGE,
                            "Ex Window", "About", WS_CHILD | WS_MAXIMIZE,
                            0 , 0, WINDOW_WIDTH, WINDOW_HEIGHT, hWnd, NULL, applicationStruct.hInstance, NULL);
                    HDC hdc1 = GetDC(hWnd);
                    TextOut(hdc1, 10, 10, "Eroshin feat. Mart\'yanov", 24);
            }
            break;
        case WM_MOUSEMOVE:
            if (GetAsyncKeyState(VK_LBUTTON)) {
                SendMessage(hWnd, WM_PAINT, PUSHED, 0);
            }
            break;
        case WM_LBUTTONUP:
            SendMessage(hWnd, WM_PAINT, UNPUSHED, 0);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);  // реакция на сообщение
            break;
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_DOWN: {
                    PostMessage(hWnd, WM_DESTROY, 0, 0);
                    break;
                }
            }
            break;
        }
        default: {
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
    }
    return 0;
}

void Draw(LPPOINT lpPoint, HDC *hdc) {
    switch (applicationStruct.brush) {
        case BRUSH_CALLIGRAPHY_PEN: {
            SetPixel(*hdc, lpPoint->x, lpPoint->y,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x + 1, lpPoint->y + 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x, lpPoint->y + 1,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x + 2, lpPoint->y + 3,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x + 2, lpPoint->y + 4,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x - 1, lpPoint->y - 1,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x - 1, lpPoint->y - 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            break;
        }
        case BRUSH_SPRAY_CAN: {
            SetPixel(*hdc, lpPoint->x, lpPoint->y,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x, lpPoint->y - 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x, lpPoint->y - 4,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x - 2, lpPoint->y - 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x - 2, lpPoint->y,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x - 4, lpPoint->y,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x - 2, lpPoint->y + 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x, lpPoint->y + 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x, lpPoint->y + 4,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x + 2, lpPoint->y + 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x +2 , lpPoint->y,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x + 4 , lpPoint->y,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            SetPixel(*hdc, lpPoint->x + 2, lpPoint->y - 2,
                     RGB(applicationStruct.red, applicationStruct.green, applicationStruct.blue));
            break;
        }
    }
}

